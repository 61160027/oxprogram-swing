
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author LIGHTsOUT
 */
public class TestWriteFile {
    public static void main(String[] args){
        FileOutputStream fos = null;
        try {
            Player o = new Player('O');
            Player x = new Player('X');
            o.win();
            x.lose();
            x.win();
            o.lose();
            x.draw();
            o.draw();
            File file =new File("ox.bin");
            fos = new FileOutputStream(file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
